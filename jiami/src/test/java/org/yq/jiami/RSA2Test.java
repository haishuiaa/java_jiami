package org.yq.jiami;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
public class RSA2Test {

	public static void main(String  [] args){
		try {
			// TODO Auto-generated method stub  
	        HashMap<String, Object> map = RSA2Util.getKeys();  
	        //生成公钥和私钥  
	        RSAPublicKey publicKey = (RSAPublicKey) map.get("public");  
	        RSAPrivateKey privateKey = (RSAPrivateKey) map.get("private"); 
	        //模  
	        String modulus = publicKey.getModulus().toString();  
	        //公钥指数  
	        String public_exponent = publicKey.getPublicExponent().toString();  
	        //私钥指数  
	        String private_exponent = privateKey.getPrivateExponent().toString();  
//	        
//	        modulus = "15396151091";
//	        public_exponent = "65537";
//	        private_exponent = "3782240369";
	        
	        System.out.println("modulus:"+modulus);
	        System.out.println("public_exponent:"+public_exponent);
	        System.out.println("private_exponent:"+private_exponent);
	        //明文  
	        String ming = "123456789中文测试sdfasdb8239498934(*#w$SDF";  
	        
	        ming = "Ja ti on ona ono mi vi oni one ona.0egnb50hue 中文测试 2342sfsdsd";
	        //使用模和指数生成公钥和私钥  
	        RSAPublicKey pubKey = RSA2Util.getPublicKey(modulus, public_exponent);  
	        RSAPrivateKey priKey = RSA2Util.getPrivateKey(modulus, private_exponent);  
	        //加密后的密文  
	        String mi = RSA2Util.encryptByPublicKey(ming, pubKey);  
	        System.err.println(mi);  
	        //解密后的明文  
	        ming = RSA2Util.decryptByPrivateKey(mi, priKey);  
	        System.err.println(ming);  
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
